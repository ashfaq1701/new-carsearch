<?php

namespace Model;

require_once 'root.inc.php';
require_once $ROOT . 'utils/database.php';

use Utils\Database;

class Model {
  public $conn;

  public function __construct() {
    $database = Database::getInstance();
    $this->conn = $database->conn;
  }
}
