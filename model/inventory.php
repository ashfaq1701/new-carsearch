<?php

namespace Model;

require_once 'model.php';

use PDO;

class Inventory extends Model {
  public function __construct() {
    parent::__construct();
  }

  public function searchInventories($zipcode, $distance, $limit, $offset) {
    $sql = 'SELECT SQL_CALC_FOUND_ROWS
        a.id as inventory_id, a.year as inventory_year, a.make as inventory_make, a.model as inventory_model, a.price as inventory_price,
        c.id as customer_id, c.customer_name as customer_name,
        u.id as zipcode_id, u.zipcode as zipcode, u.state as zipcode_state, u.city as zipcode_city, @lat:=u.latitude AS lat, @lng:=u.longitude AS lng, (6371 * acos(cos(radians(:lat)) * cos(radians(@lat)) * cos(radians(@lng) - radians(:lng)) + sin(radians(:lat)) * sin(radians(@lat)))) AS dist
        FROM inventories a
        INNER JOIN zipcodes u ON (
          a.zipcode_id = u.id AND
          (u.latitude BETWEEN :lat - (:dist / 69) AND :lat + (:dist / 69)) AND
          (u.longitude BETWEEN :lng - (:dist / (69 * COS(RADIANS(:lat)))) AND :lng + (:dist / (69 * COS(RADIANS(:lat)))))
        )
        INNER JOIN customers c ON a.customer_id = c.id
        HAVING dist < :dist
        ORDER BY dist
        LIMIT :limit
        OFFSET :offset';
    $stmt1 = $this->conn->prepare($sql);
    $stmt1->bindValue(':lat', $zipcode['latitude']);
    $stmt1->bindValue(':lng', $zipcode['longitude']);
    $stmt1->bindValue(':dist', $distance);
    $stmt1->bindValue(':limit', $limit, PDO::PARAM_INT);
    $stmt1->bindValue(':offset', $offset, PDO::PARAM_INT);
    $stmt1->execute();
    $inventories = $stmt1->fetchAll(PDO::FETCH_ASSOC);

    $stmt2 = $this->conn->prepare('SELECT FOUND_ROWS() AS foundRows');
    $stmt2->execute();
    $count = $stmt2->fetch()['foundRows'];

    return [
      'inventories' => $inventories,
      'count' => $count
    ];
  }
}
