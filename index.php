<?php

require_once __DIR__.'/model/zipcode.php';
require_once __DIR__.'/model/inventory.php';
?>
<!DOCTYPE html>
<html lang="en">
  <?php require 'templates/head.php'; ?>
  <body>
    <?php

    require 'templates/nav.php';
    setcookie('zipcode', '', time() - 86400, '/');
    setcookie('distance', '', time() - 86400, '/');
    ?>
    <div class="container-fluid">
      <div class="jumbotron top-margin">
        <form method="POST" action="/search.php">
          <div class="centered">
            <div class="top-margin">
              <label class="form-label inline-block">Zip Code</label>
              <input type="text" class="form-control filter inline-block" name="zipcode" placeholder="Enter Zip Code"/>
            </div>
            <div class="top-margin">
              <label class="form-label inline-block">Distance (miles)</label>
              <input type="number" class="form-control filter inline-block" name="distance" placeholder="Enter Distance"/>
            </div>
            <div class="top-margin">
              <button type="submit" class="btn btn-success">Search</button>
            </div>
          </div>
        </form>
      </div>
    </div>
    <?php require 'templates/javascripts.php' ?>
  </body>
</html>
